/**
 * @license MIT
 * @author Yomi Sosanya
 * @version text 
 * 
 */

(function(callback){
    if(typeof define === 'function' && !!define.amd){
        define('Whiskers',[],callback);
    }
    if(typeof window === 'object'){
        window.Whiskers = callback();
    }else if(typeof module.exports === 'object'){
        module.exports.Whiskers = callback();
    }else{
        console.error(new Error(''));
    }
})(function(){
    
    var defaults = {
        'opening': '{{', 
        'closing': '}}'
    };
    
    var entities = new Map([['\u003C','&lt;'],['\u003E','&gt;'],
        ['\u0022','&quot;'], ['\u0027','&apos;'], ['\u0026','&amp;']]);
    
    var escape = function(token, pattern=entities){
        //var token = String(token);
        var remaining = token.length, index = 0, result = '', offset = 0;
        while(remaining--){
            let curr = token[index];
            if(pattern.has(curr)){
                result += token.slice(offset,index ) + pattern.get(curr);
                offset = 1 + index;
            }
            index++;
        }
        return result + token.slice(offset,index);
    };
    
    var resolve = function(){
        // stack  processes from right to left  <----
        // token , opt_1, opt_2 ... opt_n
        var remaining = arguments.length, prev;
        if(arguments[0] === '.') return arguments[remaining -1];
        var list = arguments[0].split('.'), index = 0, cycle = list.length;
        outer: while(remaining){
            let counter = cycle, i = 0;
            prev = arguments[remaining--]; // the value of remaining is never zero
            inner: while(counter--){
                let curr = list[i++];
                if(!!prev && prev.hasOwnProperty(curr)){
                    prev = prev[curr];
                }else{
                    prev = void(0);
                    break inner;
                }
            }
            if(!!prev) break outer;
        }
        return prev;
    };
    
    var indentation = function(token,offset){
        // Removes indentation
        var pos = offset;
        while(true){
            pos--;
            if(token.charAt(pos) === '\u0020'){
            }else if((token.charAt(pos) === '\n') || (pos === -1)){
                pos++;
                break;
            }else{
                pos = offset;
                break;
            }
        }
        return pos;
    };
    
    var standalone = function(token,first,last){
        // remove newlines where apropriate
        if((token[last + 1]) === '\n' && ((token[first - 1] === '\n') || (first === 0))){
            return last + 2;
        }else if((token[last + 1] == '\r') && (token[last + 2] === '\n') && ((token[first - 1] === '\n') || (first === 0))){
            return last + 3;
        }else{
            return last + 1;
        }
    };
    
    var getTag = function*(braces,template,offset){
        var start = template.indexOf(braces['opening'],offset);
        yield start;
        var end = template.indexOf(braces['closing'], start + braces['closeLength']);
        yield end;
        var header = start + braces['openLength'];
        yield header;
    };
    
    var getLimits = function*(template,first,last,braces){
        // a wrapper that couples the functions indentation and standalone to force proper use
        var position = indentation(template, first);
        yield position;
        yield standalone(template,position,last);
    };
    
    var getComment = function*(template,opening,closing,braces){
        let last = closing + braces['closeLength'] - 1;
        yield* getLimits(template,opening,last);
    };
    
    var getDelimiter = function*(template,opening,closing,braces,header){
        if(template[closing - 1] !== '='){
            closing = template.indexOf('='+braces['closing'], closing);
            if(closing < 0) throw new SyntaxError('unclosed tag');
        }
        var token = template.slice(header + 1, closing - 1).trim();
        var pair = token.split('\u0020');
        if(pair.length !== 2 || !pair[0] || !pair[1]) throw new SyntaxError('malformed delimiter');
        braces['opening'] = pair[0];
        braces['closing'] = pair[1];
        braces['openLength'] = braces['opening'].length;
        braces['closeLength'] = braces['closing'].length;
        var last = closing + braces['closeLength'];
        yield* getLimits(template,opening,last,braces);
    };
    
    var getInterpolation = function*(template,opening,closing,braces,header,flag){
        var start, offset;
        if(flag){
            start = header;
            offset = closing + braces['closeLength'];
        }else if(template[header] === '&'){
            start = header + 1;
            offset = closing + braces['closeLength'];
        }else{
            start = header + 2;
            offset = closing + braces['closeLength'] + 1;
        }
        yield template.slice(start,closing).trim();
        yield offset;
    };
    
    var getSection = function*(template,opening,closing,braces,header,options){
        // {{# tag}} segment {{/ tag }}
        var closingStart, closingEnd, MAX = template.length - 1;
        var segOffset = closing + braces['closeLength'];
        var token = template.slice(header + 1, closing).trim();
        var loopOffset = segOffset;
        while(true){
            closingStart = template.indexOf(braces['opening']+'/', loopOffset);
            if(closingStart < 0) throw new SyntaxError('unclosed section tag '+token);
            var closingEnd = template.indexOf(braces['closing'],closingStart);
            if(closingEnd < 0) throw new SyntaxError('unclosed section tag '+token);
            let
            t = template.slice(closingStart+braces['closeLength']+1,closingEnd).trim();
            if(token === t) break;
            loopOffset = closingEnd + braces['closeLength'];
            if(loopOffset > MAX) throw SyntaxError('unclosed section tag '+token)
        }
        var last = closingEnd + braces['closeLength'] - 1;
        yield last;
        var key = resolve(token,...options);
        if(!!key){
            yield key;
            let segment = template.slice(segOffset,closingStart);
            yield segment;
        }
    };
    
    var interpret = function*(template,options,partials={},settings=defaults){
        var template = new String(template), braces = new Object, offset = 0;
        braces['opening'] = settings['opening'];
        braces['closing'] = settings['closing'];
        braces['openLength'] = braces['opening'].length;
        braces['closeLength'] = braces['closing'].length;
        iteration: while(true){
            var tag = getTag(braces,template,offset);
            var opening = tag.next().value;
            if(opening < 0) break iteration;
            var closing = tag.next().value;
            if(closing < 0) throw new SyntaxError('unclosed tag');
            var header = tag.next().value;
            if(template[header] === '!'){
                // Comments {{! comments... }}
                let comment = getComment(template,opening,closing,braces);
                yield template.slice(offset,comment.next().value);
                offset = comment.next().value;
            }else if(template[header] === '='){
                // Delimiters example: {{= [ ] =}}
                let delimiter = getDelimiter(template,opening,closing,braces,header);
                yield template.slice(offset,delimiter.next().value);
                offset = delimiter.next().value;
            }else if(template[header] === '#' || template[header] === '^'){
                // Sections and Inverted Sections
                let section = getSection(template,opening,closing,braces,header,options);
                let last = section.next().value;
                let entry = section.next();
                if((!entry.done && template[header] === '#') || 
                    (template[header] === '^' && !entry.value)){
                    let key = entry.value;
                    let segment = section.next().value;
                    if(typeof key === 'function' && template[header] === '#'){
                        yield template.slice(offset,opening);
                        yield* interpret(key(segment),options,partials,braces);
                        offset = last + 1;
                    }else if(!!key && typeof key[Symbol.iterator] === 'function' &&
                        template[header] === '#'){
                        // List
                        let iter = key[Symbol.iterator](), entry = iter.next();
                        yield template.slice(offset,opening);
                        while(!entry.done){
                            let value = entry.value;
                            options.push(value);
                            yield* interpret(segment,options,partials,braces);
                            options.pop();
                            entry = iter.next();
                        }
                        offset = last + 1;
                    }else if(typeof key === 'object' && template[header] === '#'){    
                        options.push(key);   
                        yield template.slice(offset,opening);    
                        yield* interpret(segment,options,partials,braces);    
                        options.pop();    
                        offset = last + 1;
                    }else{
                        // boolean and truthy    
                        yield template.slice(offset,opening);    
                        yield* interpret(segment,options,partials,settings);    
                        offset = last + 1;
                    }
                }else{
                    // Misses and truthy inverted section
                    let limits = getLimits(template,opening,last,braces);
                    yield template.slice(offset,limits.next().value);
                    offset = limits.next().value;
                }
            }else if(template[header] === '>'){
                // Partials {{> tag }}
                let token = template.slice(opening + braces['openLength'] + 1,closing).trim();
                let segment = partials[token];
                let last = closing + braces['closeLength'] - 1;
                if(segment !== undefined){
                    yield template.slice(offset, opening);
                    yield* interpret(segment,options,partials,settings);
                    offset = last + 1;
                }else{
                    let limits = getLimits(template,opening,last,braces);
                    yield template.slice(offset, limit.next().value);
                    offset = limits.next().value;
                }
            }else{
                // Interpolation {{ tag }}
                let escapeFlag = !(template[header]=== '&' || 
                (template[header] === '{' && template[header+1] === '{' && 
                template[header+2] === '{' && template[closing] ==='}') && 
                template[closing+1] === '}' && template[closing+2] === '}');
                // triple mustaches can only be used, if and only, the opening and
                // 'closing braces are '{{' and '}}' (mustache's defaults) respectively.
                let iterator = getInterpolation(template,opening,closing,braces,header,escapeFlag);
                let key = resolve(iterator.next().value,...options);
                if(typeof key === 'string'/* || (typeof key === 'number' && Number.isFinite(key))*/){
                    yield template.slice(offset,opening);
                    yield (escapeFlag)? escape(key, entities) : key;
                    offset = iterator.next().value;
                }else if(typeof key === 'function'){
                    let result = key();
                    yield template.slice(offset,opening);
                    yield* interpret(key(),options,partials,settings);
                    offset = iterator.next().value;
                }else{
                    if(!key){
                        // Rule : The Interpolation tags MUST NOT be treated as standalone.
                        let last = iterator.next().value - 1;
                        yield template.slice(offset,opening);
                        offset = last + 1;
                    }else{
                        // typeof key === 'object' or any other truthy object.
                        yield template.slice(offset,opening);
                        yield key;
                        offset = iterator.next().value;
                    }
                }
            }
        } // end of iteration (while)
        yield template.slice(offset);
    };
    
    return {
        'interpreter': interpreter,
        'escape' : escape
    };


    
})



